import os
import glob
import codecs

'''
pivot for depths except for the ones with dates

 {'confidence': 1.0, 'encoding': 'ascii'}

 DATE,A_P,DEPTH_1M,DEPTH_2M,DEPTH_3M,DEPTH_4M,DEPTH_5M,DEPTH_7M,DEPTH_9M,DEPTH_11M,DEPTH_13M,DEPTH_14M,DEPTH_15M,DEPTH_17M

 OR

 Depth,24/06/1994,24/06/1994,26/09/1995
 Depth (m),28/09/1995,05/07/1995

 shift to date, depth, a_p (if exists)

 pull the depth value from the header

'''

datasets = glob.glob('../ggd700/*.dat')

for dataset in datasets:
	print dataset
	with open(dataset, 'r') as f:
		data = f.readlines()

	#chuck the randomly empty bits at the end (too many commas)
	headers = [d.strip() for d in data[0].split(',') if d]
	records = [d.strip().split(',') for d in data[1:] if d]

	newrecs = []

	if 'date' in headers[0].lower():
		# this looks like the only one with the a_p field
		for rec in records:
			date = rec[0]
			
			depth_index = headers.index('A_P') + 1 if 'A_P' in headers else 1
			a_p = rec[headers.index('A_P')] if 'A_P' in headers else ''

			for header in headers[depth_index:]:
				depth = header.split('_')[-1].replace('M', '')

				value = rec[headers.index(header)]

				newrecs.append(','.join([date, depth, value, a_p]))

	elif 'depth' in headers[0].lower():
		for rec in records:
			depth = rec[0]

			for header in headers[1:]:
				# date = header
				# value = value at the header index
				if not header:
					continue

				value = rec[headers.index(header)]
				newrecs.append(','.join([header, depth, value, '']))

	else:
		print 'no idea what is happening here', headers
		continue

	with open(os.path.join('..', 'csvs', 'ggd700', dataset.split('/')[-1].replace('.dat', '.csv')), 'w') as f:
		f.write('Date, Depth (m), Value, A_P\n')
		f.write('\n'.join(newrecs))