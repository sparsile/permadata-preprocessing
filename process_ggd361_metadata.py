import os
import glob
import codecs


'''
parse README file to pull out site descriptions
and measurement info.

output as one file with all site info

NOTE: this has some output encoding issues
'''

bom = codecs.BOM_UTF16_LE

metadatas = []

datasets = glob.glob('../ggd361/*/')
for dataset in datasets:
	datafiles = glob.glob(os.path.join(dataset, '*', '*_base_pub_ReadMe.txt'))
	for datafile in datafiles:
		print 'parsing', datafile

		filename = datafile.split('/')[-1].split('.')[0]
		parts = filename.split('_')

		SITE = parts[0]
		DATACODE = parts[1]

		data = codecs.open(datafile, 'r', encoding='UTF-16LE').read()
		data = data[len(bom)-1:].split('\r\n')	
		data = [d.strip() for d in data if d and '-----------' not in d]

		site_index = data.index('SITE DE MESURE / MEASUREMENT SITE')
		desc_index = data.index('DESCRIPTION DU SITE / SITE DESCRIPTION')
		measure_index = data.index('MESURE / MEASUREMENT')

		#this is the end of the blob and may not exist in all files
		try:
			history_index = data.index('HISTORIQUE DE L\'INSTRUMENTATION / INSTRUMENT HISTORY')
		except:
			history_index = data.index('SOUS-STATION / SUBSTATION')

		site_lines = data[site_index + 1:desc_index]

		type_line = [s for s in site_lines if 'Type :' in s]
		TYPE = type_line[0].split(':')[-1].split('/')[-1].strip() if type_line else ''

		name_line = [s for s in site_lines if 'Nom / ' in s]
		NAME = name_line[0].split(':')[-1].strip() if name_line else ''

		local_line = [s for s in site_lines if 'Localisation' in s]
		LOCATION = local_line[0].split(':')[-1].strip() if local_line else ''

		lat_line = [s for s in site_lines if 'Latitude :' in s]
		LAT = lat_line[0].split(':')[-1].strip() if lat_line else ''

		lng_line = [s for s in site_lines if 'Longitude :' in s]
		LNG = lng_line[0].split(':')[-1].strip() if lng_line else ''

		altitude_line = [s for s in site_lines if 'Altitude' in s]
		ALTITUDE = altitude_line[0].split(':')[-1].strip() if altitude_line else ''

		start_line = [s for s in site_lines if 'Mise en service' in s]
		START_DATE = start_line[0].split(':')[-1].split('/')[-1].strip() if start_line else ''

		end_line = [s for s in site_lines if 'Fermeture' in s]
		END_DATE = end_line[0].split(':')[-1].split('/')[-1].strip() if end_line else ''

		desc_lines = data[desc_index + 1:measure_index]
		DESCRIPTION = '. '.join([d.strip() for d in desc_lines if d]).replace('.. ', '. ')

		measure_lines = data[measure_index + 1:history_index]
		param_line = [s for s in measure_lines if 'Type de' in s]
		PARAMETER = param_line[0].split(':')[-1].split('/')[-1].strip() if param_line else ''

		unit_line = [s for s in measure_lines if 'Unit' in s and 'mesures' in s]
		UNITS = unit_line[0].split(':')[-1].split('/')[-1].strip() if unit_line else ''

		paramdesc_line = [s for s in measure_lines if 'Description' in s]
		PARAMETER_DESCRIPTION = paramdesc_line[0].split(':')[-1].split('/')[-1].strip() if paramdesc_line else ''

		locale_line = [s for s in measure_lines if 'Toutes les dates' in s]
		LOCALE = locale_line[0].split(':')[-1].split('/')[-1].strip() if locale_line else ''

		timeperiod_line = [s for s in measure_lines if ' couverte ' in s]
		TIMEPERIOD = timeperiod_line[0].split(':')[-1].split('/')[-1].strip() if timeperiod_line else ''

		metadatas.append('|'.join([
				SITE,
				DATACODE,
				TYPE,
				NAME,
				LOCATION,
				LAT,
				LNG,
				ALTITUDE,
				START_DATE,
				END_DATE,
				DESCRIPTION,
				PARAMETER,
				UNITS,
				PARAMETER_DESCRIPTION,
				LOCALE,
				TIMEPERIOD
			]).encode('utf-16'))

#print metadatas

#save the site id, data code, type, name, location, lat, lng, altitude, start date, end date, 
#	site description, measurement type, units, description, datetime note, timeperiod for data collection
with open(os.path.join('..','metadata','ggd361', 'metadata.csv'), 'w') as f:
	f.write('site id|data code|type|name|location|lat|lng|altitude|start date|end date|site description|measurement type|units|description|datetime note|timeperiod for data collection\n')
	f.write('\n'.join(metadatas))









