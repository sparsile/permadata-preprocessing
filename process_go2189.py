import os
import glob

'''
add month field if missing
pivot depth values

Output: Year, Month, Depth, Borehole Temp (C)
Output 2: Year, month, Surface Temp (x2 if both present)


data records start at YEAR,
empty line between header and data
'''

datasets = glob.glob('../go2189/working/*.csv')

for dataset in datasets:
	print dataset
	with open(dataset, 'r') as f:
		lines = f.read().split('\r')

	filename = dataset.split('/')[-1].split('.')[0]
	month = '1' if 'January' in filename else '7'

	header_line = [l for l in lines if l[0:5] == 'YEAR,']
	if not header_line:
		print '\tunexpected headers!'
		continue

	headers = lines[lines.index(header_line[0])].split(',')
	records = lines[lines.index(header_line[0]) + 1:]
	records = [r.strip().split(',') for r in records if r]

	newrecs = []
	new_surfaces = []
	surface_headers = [h for h in headers if 'surface' in h and 'temp' in h]
	for rec in records:
		if not rec[0]:
			#assuming this is the blank row or the odd rows with no year (or much data)
			continue

		year = rec[0]
		data_index = 2 if 'MONTH' in headers else 1

		surface_temps = {}
		
		for header in headers[data_index:]:
			if ' m' in header:
				# it's a depth value
				#add data as year, month, depth 
				depth = header.split(' ')[0]
				newrecs.append(','.join([
						year, 
						month,
						depth,
						rec[headers.index(header)]
					]))
			elif header in surface_headers:
				# it's a surface temperature value
				surface_temps[header] = rec[headers.index(header)]
			else:
				#there's another field?!
				print header, headers
				continue

		#deal with the surface temps (there's only two choices)
		if surface_temps:
			new_surfaces.append(','.join([
					year, 
					month
					] + [surface_temps[i] for i in surface_headers]
				))	

	with open(os.path.join('..', 'csvs', 'go2189', filename + '.csv'), 'w') as f:
		f.write('Year,Month,Depth_m,Temp_C\n')
		f.write('\n'.join(newrecs))

	if new_surfaces:
		with open(os.path.join('..', 'csvs', 'go2189', filename + '_surface_temps.csv'), 'w') as f:
			f.write(','.join(['Year', 'Month'] + surface_headers) + '\n')
			f.write('\n'.join(new_surfaces)) 

