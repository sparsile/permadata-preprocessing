import os
import glob

'''
fixed width, no headers to csv with headers

year, active layer thickness
'''

datasets = glob.glob('../arcss160/arcss160/*.max')

for dataset in datasets:
	print dataset
	with open(dataset, 'r') as f:
		lines = f.readlines()

	newlines = []
	for line in lines:
		if not line:
			continue
		newlines.append(','.join([
				line[0:17].strip(),
				line[17:].strip()
			]))


	with open(os.path.join('..', 'csvs', 'arcss160', dataset.split('/')[-1].replace('.max', '.csv')), 'w') as f:
		f.write('Year,ActiveLayerThickness\n')
		f.write('\n'.join(newlines))	