import os
import csv
import subprocess
import zipfile
import glob
import codecs


'''
for each directory (a dataset)
	for each zip (site)
		unzip
		for each pub.txt (data file)
			repack as csv from fixed width structure

stated structure:
	Caracteres / Characters		Valeur / Value
	00-05				Code de donnees / Data code
	06-12				Code de station / Station code
	13-21				Latitude
	22-30				Longitude
	31-34				Annee / Year
	35-36				Mois / Month
	37-38				Jour / Day
	39-39				Balise de debut de l'heure / Time delimiter(ISO-8601) 
	40-41				Heure / Hour
	42-43				Minute
	44-63				Code de sous-station / Substation code
	64-65				Code de qualite / Quality code
	66 + 				Valeur mesuree / Measured data

	Code de donnees manquantes  / Missing data code : -99999

example line:
00054BYLOTPB+07315633-0799855019990616T2100                0_CM  5.96500

example output:
00054,BYLOTPB,+07315633,-07998550,1999,06,16,T,21,00,                0_CM,  ,5.96500

(0_CM is the substation)
'''

bom = codecs.BOM_UTF16_LE

datasets = glob.glob('../ggd361/*/')
#datasets = ['../ggd361/10.5885 + 45291SL-34F28A9491014AFD']
for dataset in datasets:
	parts = dataset.split('/')
	zips = glob.glob(os.path.join(dataset, '*.zip'))
	for zname in zips:
		zipdir = '/'.join(zname.replace('.zip', '').split('/'))
		if not os.path.exists(zipdir):
			with zipfile.ZipFile(zname, 'r') as z:
				z.extractall(zipdir)

		#read and parse the datafiles
		datafiles = glob.glob(os.path.join(zipdir, '*_base_pub.txt'))
		for datafile in datafiles:
			print 'parsing', datafile

			outpath = os.path.join('..', 'csvs', parts[1], parts[2])

			if not os.path.exists(outpath):
				os.makedirs(outpath)

			outfile = os.path.join(outpath, datafile.split('/')[-1].replace('.txt', '.csv'))

			parsed_data = [','.join(['DataCode', 'StationCode', 'Latitude', 'Longitude', 'Year', 'Month', 'Day', 'Delim', 'Hour', 'Minute', 'SubstationCode', 'QualityCode', 'DataValue'])]
			data = codecs.open(datafile, 'r', encoding='UTF-16LE').read()
			data = data[len(bom)-1:].split('\r\n')
			for line in data:
				aline = line.strip()
				if not aline:
					continue
				parsed_data.append(','.join([
					aline[0:5], 
					aline[5:12], 
					aline[12:21],
					aline[21:30],
					aline[30:34],
					aline[34:36],
					aline[36:38],
					aline[38:39],
					aline[39:41],
					aline[41:43],
					aline[43:63].strip(),
					aline[63:65],
					aline[65:].strip()
					])
				)

			with open(outfile, 'w') as f:
				f.write('\n'.join(parsed_data))




