import os
import glob
import codecs

'''
repack data from 'singles' which i believe is the combined data from 
the other *_files directories (fingers-crossed)
to remove the metadata header (to go in site description files) and
convert to csv

data headers:
site  log measured vert temperature
no     no depth    depth
214 19   1     .3     .3   1.400

where site no is a two part bit of text (214 and 19 together, 
	19 is the well number)

'''

datasets = glob.glob('../ggd503/single/*.dat')
#datasets = ['../ggd503/single/ca214_19.dat']

for dataset in datasets:
	print dataset

	data = codecs.open(dataset, 'r', encoding='ISO-8859-2').read()
	
	filename = dataset.split('/')[-1]

	#split on \n but strip out \r just in case
	lines = data.split('\n')

	data_index = lines.index('site  log measured vert temperature')	
	if data_index < 0:
		print '\tincompatible file!'
		continue
	
	#get data from the two header lines on
	data_lines = lines[data_index+2:]	

	newlines = []
	for data_line in data_lines:
		line = data_line.strip()
		if not line:
			continue

		#fun with encoding?
		#strip out the oddball chars at the end if the line also contains 
		#a second newline
		line = line.split('\r')[0]

		newlines.append(','.join([
				line[0:6],
				line[6:11].strip(),
				line[11:18].strip(),
				line[18:25].strip(),
				line[25:].strip()
			]))

	with open(os.path.join('..', 'csvs', 'ggd503', filename.replace('.dat', '.csv')), 'w') as f:
		f.write('site no, log no, measured depth, vert depth, temperature\n')
		f.write('\n'.join(newlines))
