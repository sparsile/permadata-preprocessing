import os
import glob
import subprocess

#NOTE: this, hilariously and in ways i am not poking around at today,
#      provides a different total based on totals and on the sum of 
#      line count per file

#run from within the dataset csv folder
datasets = glob.glob('*/')
raw_counts = []

print 'num dirs: ', len(datasets)
print datasets

for dataset in datasets:
	#cd into the thing
	chpath = '/Users/sscott/Documents/docs/permadata/csvs/ggd361/' + dataset
	os.chdir(chpath)

	print os.getcwd()

	s = subprocess.Popen('find . -name \'*.csv\' | xargs wc -l', shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	response, err = s.communicate()
	
	print response

	recs = response.split('\n')

	print '\t', len([r for r in recs if r])
	for rec in recs:
		if not rec:
			continue
		parts = rec.strip().split(' ')
		raw_counts.append(','.join([dataset, parts[1], parts[0]]))

		# if parts[1] == 'total':
		# 	totals.append(int(parts[0]))
		# else:
		# 	raw_counts.append(','.join([dataset, parts[1], parts[0]]))


with open('/Users/sscott/Documents/docs/permadata/ggd361_records.csv', 'w') as f:
	f.write('dataset,file,count\n')
	f.write('\n'.join(raw_counts))



