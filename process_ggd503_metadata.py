import os
import glob
import codecs

'''
pulling the log and site description from the "single" files
and creating a site description file plus a combined log file (i am lazy)
'''

datasets = glob.glob('../ggd503/single/*.dat')

descriptions = []
logs = []

for dataset in datasets:
	print dataset

	data = codecs.open(dataset, 'r', encoding='ISO-8859-2').read()
	
	filename = dataset.split('/')[-1]

	#split on \n but strip out \r just in case
	lines = data.split('\n')
	lines = [d.strip() for d in lines if d]

	log_index = lines.index('site   log   type of log       date     accuracy  comments')
	data_index = lines.index('site  log measured vert temperature')

	#get the site descritpion block
	description_lines = lines[0:log_index]

	#this is silly. i don't care.
	site_line = [d for d in description_lines if 'site number:' in d]
	SITE = site_line[0].split(':')[-1].strip() if site_line else ''

	well_line = [d for d in description_lines if 'well number:' in d]
	WELL = well_line[0].split(':')[-1].strip() if well_line else ''

	name_line = [d for d in description_lines if 'site name:' in d]
	NAME = name_line[0].split(':')[-1].strip() if name_line else ''

	industry_line = [d for d in description_lines if 'industry name:' in d]
	INDUSTRY = industry_line[0].split(':')[-1].strip() if industry_line else ''

	lat_line = [d for d in description_lines if 'latitude' in d]
	LAT = lat_line[0].split(':')[-1].strip() if lat_line else ''

	lng_line = [d for d in description_lines if 'longitude' in d]
	LNG = lng_line[0].split(':')[-1].strip() if lng_line else ''

	elev_line = [d for d in description_lines if 'ground elev' in d]
	ELEVATION = elev_line[0].split(':')[-1].strip() if elev_line else ''

	kb_line = [d for d in description_lines if 'KB above ground' in d]
	KB = kb_line[0].split(':')[-1].strip() if kb_line else ''

	depth_line = [d for d in description_lines if 'total Depth' in d]
	DEPTH = depth_line[0].split(':')[-1].strip() if depth_line else ''

	start_line = [d for d in description_lines if 'drill start' in d]
	START_DATE = start_line[0].split(':')[-1].strip() if start_line else ''
	
	end_line = [d for d in description_lines if 'drilling end' in d]
	END_DATE = end_line[0].split(':')[-1].strip() if end_line else ''

	abandon_line = [d for d in description_lines if 'well abandon' in d]
	ABANDON_DATE = abandon_line[0].split(':')[-1].strip() if abandon_line else ''

	ipa_line = [d for d in description_lines if 'IPA/GGD data set' in d]
	IPA = ipa_line[0].split(':')[-1].strip() if ipa_line else ''

	investigator_line = [d for d in description_lines if 'Investigators' in d]
	INVESTIGATORS = investigator_line[0].split(':')[-1].strip() if investigator_line else ''

	institute_line = [d for d in description_lines if 'Institute' in d]
	INSTITUTE = institute_line[0].split(':')[-1].strip() if institute_line else ''

	ref_line = [d for d in description_lines if 'Key Reference' in d]
	REFERENCE = ref_line[0].split(':')[-1].strip() if ref_line else ''

	#get the second line of the reference?
	ref_index = description_lines.index(ref_line[0])
	REFERENCE += ' ' + description_lines[ref_index + 1].strip()


	descriptions.append(','.join([
			SITE,
			WELL.replace(',', ';'),
			NAME.replace(',', ';'),
			INDUSTRY.replace(',', ';'),
			LAT,
			LNG,
			ELEVATION,
			KB,
			DEPTH,
			START_DATE,
			END_DATE,
			ABANDON_DATE,
			IPA.replace(',', ';'),
			INVESTIGATORS.replace(',', ';'),
			INSTITUTE.replace(',', ';'),
			REFERENCE.replace(',', ';')
		]))

	#get the log description block
	log_lines = lines[log_index + 2:data_index]

	for log_line in log_lines:
		if not log_line.strip():
			continue

		logs.append(','.join([
				log_line[0:6],
				log_line[6:10].strip(),
				log_line[10:28].strip(),
				log_line[28:38].strip(),
				log_line[38:45].strip(),
				log_line[45:49].strip(),
				log_line[49:].strip().replace(',', ';')
			]))


with open('../metadata/ggd503/site_descriptions.csv', 'w') as f:
	f.write('Site Number, Well Number, Site Name, Industry Name, Latitude, Longitude, Elevation (M), KB above ground, Total Depth (m), Date of drill start, Date drilling end, Date well abandonment, IPA/GGD, Investigators, Institute, Reference\n')	
	f.write('\n'.join(descriptions))

with open('../metadata/ggd503/log_descriptions.csv', 'w') as f:
	f.write('Site Number, Log Number, Type of Log, Date of log, Accuracy (depth), Accuracy (temp), Comments\n')
	f.write('\n'.join(logs))